﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_5.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required]
        [StringLength(500, ErrorMessage = "Nhập từ 20-500 ký tự"), 
        MinLength(5, ErrorMessage = "Nhập từ 20-500 ký tự")]
        public String Title { set; get; }

        [Required]
        [MinLength(5, ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [Required]
        [DataType(DataType.DateTime,ErrorMessage="Nhập ngày tháng DD/MM/YYY")]
        public DateTime DateCreated { set; get; }
        
        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày tháng DD/MM/YYY")]
        public DateTime DateUpdated { set; get; }

        //Tạo quan hệ với User đăng bài
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }

        //Tạo quan hệ với Comment
        public virtual ICollection<Comment> Comments { set; get; }

        //Tạo quan hệ với Tag
        public virtual ICollection<Tag> Tags { set; get; }

    }
}