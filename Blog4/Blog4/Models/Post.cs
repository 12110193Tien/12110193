﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [StringLength(500, ErrorMessage = "so ky tu trong khoang 20-500", MinimumLength = 20)]
        public string Title { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [StringLength(5000, ErrorMessage = "so ky tu tối thiểu 50", MinimumLength = 50)]
        public string Body { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }


        public int UserProfileUserID { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}