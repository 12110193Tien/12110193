﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LSLOL4.Models
{
    public class Post
    {
        [Required]

        public int PostID { set; get; }

        [StringLength(500, ErrorMessage = "Vui lòng nhập số lượng ký tự trong khoảng (10,500)", MinimumLength = 10)]
        public String Title { set; get; }
        [StringLength(3000, ErrorMessage = "Số lượng kí tự phải trong khoảng 10-3000", MinimumLength = 10)]
        public String Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime PostCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public int TagID { set; get; }
        public virtual Tag Tag { set; get; }

    }
}