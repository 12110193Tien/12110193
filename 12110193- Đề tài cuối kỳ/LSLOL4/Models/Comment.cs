﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LSLOL4.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public string Body { set; get; }
        public DateTime CommentCreated { set; get; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }

        //public LastTime LastTime { get; set; }
    }
}