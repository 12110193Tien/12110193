﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LSLOL4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Liên Minh Đem Sự Thông Minh Sáng Tạo Đến Cho Mọi Người";

            return View();
        }
        public ActionResult _addTuong()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Tải Game LOL";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Mọi Chi Tiết Liên Hệ";

            return View();
        }
    }
}
