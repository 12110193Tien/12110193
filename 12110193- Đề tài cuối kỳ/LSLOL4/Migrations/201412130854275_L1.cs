namespace LSLOL4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class L1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 500),
                        Body = c.String(maxLength: 1000),
                        PostCreated = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PostID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.UserProfileUserId)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        CommentCreated = c.DateTime(nullable: false),
                        PostID = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: false)
                .Index(t => t.PostID)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "UserProfileUserId" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "TagID" });
            DropIndex("dbo.Posts", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.Comments", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Posts", "UserProfileUserId", "dbo.UserProfile");
            DropTable("dbo.Tags");
            DropTable("dbo.Comments");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfile");
        }
    }
}
