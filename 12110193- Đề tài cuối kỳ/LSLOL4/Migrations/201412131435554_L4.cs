namespace LSLOL4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class L4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Body", c => c.String(maxLength: 3000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Body", c => c.String(maxLength: 1000));
        }
    }
}
