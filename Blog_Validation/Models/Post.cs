﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{      
    [Table("BaiViet")]

    public class Post
    {
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "số lượng ký tự trong khoảng 20 đến 500", MinimumLength = 20)]
        public String Title { set; get; }
        [StringLength(1000000000, ErrorMessage = "số lượng ký tự tối thiểu là 50", MinimumLength = 50)]
        public String Body { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Vui lòng nhập đúng ngày tháng năm")]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.DateTime, ErrorMessage = "Vui lòng nhập đúng ngày tháng năm")]
        public DateTime DateUpdated { set; get; }
        

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        
        

    }
}