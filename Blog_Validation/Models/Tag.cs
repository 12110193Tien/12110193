﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Blog_2.Models
{
    public class Tag
    {
        [Required]

        public int TagID { set; get; }
        [StringLength(100, ErrorMessage="số lượng ký tự trong khoảng 10 đến 100", MinimumLength=10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}