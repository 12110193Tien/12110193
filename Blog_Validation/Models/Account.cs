﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
        [Required]
        [DataType(DataType.Password)]
        public int Password { set; get; }
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }

        [StringLength(100, ErrorMessage = "Vui lòng nhập không được quá 100 ký tự")]
        public String Firstname { set; get; }

        [StringLength(100, ErrorMessage = "Vui lòng nhập không được quá 100 ký tự")]
        public String Lastname { set; get; }
       
        public int AccountID { set; get; }
        
    }
}