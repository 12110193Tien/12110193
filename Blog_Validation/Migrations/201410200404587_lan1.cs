namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.BaiViet", "Body", c => c.String());
            AlterColumn("dbo.Tags", "Content", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.BaiViet", "Body", c => c.String(maxLength: 500));
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false));
        }
    }
}
