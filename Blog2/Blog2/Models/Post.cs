﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Post
    {
        [Required]
        public int ID { set; get; }

        [Required]
        [StringLength(500, ErrorMessage = "So luong ki tu tu 20-500 ki tu", MinimumLength = 20)]
        public string Title { set; get; }

        [Required]
        [MinLength(50, ErrorMessage = "it nhat 50 ki tu")]
        public string Body { set; get; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdate { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        //public int TagID { set; get; }
        //public virtual Tag Tag { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}