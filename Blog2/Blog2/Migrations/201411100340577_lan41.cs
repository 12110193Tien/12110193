namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan41 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TagPosts", "Tag_ID", "dbo.Tags");
            DropForeignKey("dbo.TagPosts", "Post_ID", "dbo.Posts");
            DropIndex("dbo.TagPosts", new[] { "Tag_ID" });
            DropIndex("dbo.TagPosts", new[] { "Post_ID" });
            DropTable("dbo.Tags");
            DropTable("dbo.TagPosts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TagPosts",
                c => new
                    {
                        Tag_ID = c.Int(nullable: false),
                        Post_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_ID, t.Post_ID });
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.TagPosts", "Post_ID");
            CreateIndex("dbo.TagPosts", "Tag_ID");
            AddForeignKey("dbo.TagPosts", "Post_ID", "dbo.Posts", "ID", cascadeDelete: true);
            AddForeignKey("dbo.TagPosts", "Tag_ID", "dbo.Tags", "ID", cascadeDelete: true);
        }
    }
}
