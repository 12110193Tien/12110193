namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan31 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "DateUpdate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Comments", "DateUpdate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
            DropColumn("dbo.Comments", "DateUpdate");
            DropColumn("dbo.Posts", "DateUpdate");
        }
    }
}
